# Pearl

> A game from Simon Tatham's Portable Puzzle Collection

## Howto

Draw a single closed loop by connecting together the centres of adjacent grid squares, so that some squares end up as corners, some as straights (horizontal or vertical), and some may be empty. Every square containing a black circle must be a corner not connected directly to another corner; every square containing a white circle must be a straight which is connected to at least one corner.

Drag between squares to draw or undraw pieces of the loop. Alternatively, left-click the edge between two squares to turn it on or off. Right-click an edge to mark it with a cross indicating that you are sure the loop does not go through it.

## Develop

Start locally by running `npm start`, which serves the content at `http://localhost:3000`.
